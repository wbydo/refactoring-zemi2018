package org.speech_lab.refactoring_zemi2018.chapter9;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.io.*;

class SeasonCharge
{
    private int _summerRate = 3;
    private int _winterRate = 2;
    private int _winterServiceCharge = 2;

    private Date _today;
    private double _quantity;

    private static Date SUMMER_START = toDate("2018/06/01");
    private static Date SUMMER_END = toDate("2018/08/31");

    public SeasonCharge(double quantity, String toDay) {
        Date date = toDate(toDay);

        _today = date;
        _quantity = quantity;
    }

    public static Date toDate(String str) {
        // 日付フォーマットを作成
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        // Date型へ変換
        try {
            return dateFormat.parse(str);
        } catch ( ParseException e ) {
            return null;
        }
    }

    double getCharge() {
        return isSummer(_today) ? summerCharge() : winterCharge();
    }

    private double winterCharge(){
        return _quantity * _winterRate + _winterServiceCharge;
    }

    private double summerCharge(){
        return _quantity * _summerRate;
    }

    Date getDate()
    {
        return _today;
    }

    boolean isSummer(Date date){
        boolean result = date.after(SUMMER_START) && date.before(SUMMER_END);
        return result;
    }
}
