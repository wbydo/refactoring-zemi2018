package org.speech_lab.refactoring_zemi2018.chapter9;

class NullStudent extends Student {
    //デフォルトとは？とは考えないことにする
    public static double DEFAULT_GPA = 2.0;

    public NullStudent(String name, double gpa){
        super(name, gpa);
    }

    public NullStudent(){}

    @Override
    public boolean isNull(){
        return true;
    }

    @Override
    public double getGPA(){
        return DEFAULT_GPA;
    }
}
