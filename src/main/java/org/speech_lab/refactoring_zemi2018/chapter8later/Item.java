package org.speech_lab.refactoring_zemi2018.chapter8later;

public class Item {
    private final ItemType _type;

    private final String _title;
    private final int _price;

    public Item(ItemType type, String title, int price) {
        _type = type;
        _title = title;
        _price = price;
    }

    public int getTypecode() {
        return _type.getTypecode();
    }

    public String getTitle() {
        return _title;
    }

    public int getPrice() {
        return _price;
    }

    public String toString() {
        return "[ "
            + getTypecode() + ", "
            + getTitle() + ", "
            + getPrice() + " ]";
    }
}
