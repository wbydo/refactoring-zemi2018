package org.speech_lab.refactoring_zemi2018.chapter8later;

import org.speech_lab.refactoring_zemi2018.chapter8later.employee.Engineer;
import org.speech_lab.refactoring_zemi2018.chapter8later.employee.Salesman;
import org.speech_lab.refactoring_zemi2018.chapter8later.employee.Manager;

abstract public class Employee {
    public static final int ENGINEER = 0;
    public static final int SALESMAN = 1;
    public static final int MANAGER = 2;

    abstract public int getType();

    public static Employee create(int type){
        switch(type){
            case ENGINEER:
                return new Engineer();
            case SALESMAN:
                return new Salesman();
            case MANAGER:
                return new Manager();
            default:
                return null;
        }
    }

    abstract public String getCategory();
}
