package org.speech_lab.refactoring_zemi2018.chapter6;

class Sales {
  private int _itemPrice;
  private int _quantity;

  public Sales(int itemPrice, int quantity) {
    _itemPrice = itemPrice;
    _quantity = quantity;
  }

  private int calcBasePrice(){
    return _quantity * _itemPrice;
  }

  private double calcDiscountFactor(){
    if(calcBasePrice() > 1000){
      return 0.95;
    } else {
      return 0.98;
    }
  }

  private double calcQuantityDiscount(){
    return Math.max(0, _quantity - 500) * _itemPrice * 0.05;
  }

  private double calcShipping(){
    return Math.min(_quantity * _itemPrice * 0.1, 100.0);
  }

  double getPrice() {
    return calcBasePrice() * calcDiscountFactor();
  }

  double price() {
    return calcBasePrice() - calcQuantityDiscount() + calcShipping();
  }
}
