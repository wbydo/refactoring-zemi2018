package org.speech_lab.refactoring_zemi2018.chapter8early;

import java.util.Map;
import java.util.HashMap;

class Customer {
    private final String _name;

    private static Map<String, Customer> _instances = new HashMap();

    private Customer(String name){
        _name = name;
    }

    String getName(){
        return _name;
    }

    public static Customer getBy(String name){
        return _instances.get(name);
    }

    public static void loadCustomer(){
        new Customer("直人").store();
        new Customer("一平").store();
        new Customer("ゆか").store();
    }

    private void store() {
        _instances.put(getName(), this);
    }
}
