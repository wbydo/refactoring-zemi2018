package org.speech_lab.refactoring_zemi2018.chapter8early;

import java.awt.Frame;
import java.awt.TextField;
import java.awt.BorderLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusAdapter;

import java.util.Observer;
import java.util.Observable;

public class IntervalWindow extends Frame implements Observer {
    private static final int MAXLEN = 10;
    private Interval _subject;

    TextField _startField;
    TextField _endField;
    TextField _lengthField;

    public IntervalWindow() {
        super("Interval Window");

        _startField = new TextField("0", MAXLEN);
        _endField = new TextField("0", MAXLEN);
        _lengthField = new TextField("0", MAXLEN);

        _startField.addFocusListener(new SymFocus());
        _endField.addFocusListener(new SymFocus());
        _lengthField.addFocusListener(new SymFocus());

        add(_startField, BorderLayout.NORTH);
        add(_endField, BorderLayout.CENTER);
        add(_lengthField, BorderLayout.SOUTH);

        pack();
        show();

        _subject = new Interval();
        _subject.addObserver(this);
        update(_subject, null);
    }

    class SymFocus extends FocusAdapter {
        public void focusLost(FocusEvent event) {
            Object object = event.getSource();
            if (object == _startField) {
                startFieldFocusLost(event);
            } else if (object == _endField) {
                endFieldFocusLost(event);
            } else if (object == _lengthField) {
                leengthFieldFocusLost(event);
            }
        }
    }

    void startFieldFocusLost(FocusEvent event) {
        String s = _startField.getText();
        setStart(s);
        calculateLength();
    }

    void endFieldFocusLost(FocusEvent event) {
        String s = _endField.getText();
        setEnd(s);
        calculateLength();
    }

    void leengthFieldFocusLost(FocusEvent event) {
        String s = _lengthField.getText();
        setLength(s);
        calculateEnd();
    }

    void calculateLength() {
        _subject.calculateLength();
    }

    void calculateEnd() {
        _subject.calculateEnd();
    }

    void setStart(String s){
        _subject.setStart(s);
    }

    String getStart(){
        return _subject.getStart();
    }

    void setEnd(String s){
        _subject.setEnd(s);
    }

    String getEnd(){
        return _subject.getEnd();
    }

    void setLength(String s){
        _subject.setLength(s);

    }

    String getLength(){
        return _subject.getLength();
    }

    public void update(Observable o, Object arg){
        _startField.setText(_subject.getStart());
        _endField.setText(_subject.getEnd());
        _lengthField.setText(_subject.getLength());
    }
}
