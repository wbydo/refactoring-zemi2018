package org.speech_lab.refactoring_zemi2018.chapter7;

public class Person {
    private String _name;
    private Department _department;
    private TelephoneNumber _officeTelephoneNumber;

    public Person(String name){
        _name = name;
        _officeTelephoneNumber = new TelephoneNumber();
    }


    public String getName(){
        return _name;
    }

    public String getTelephoneNumber(){
        return _officeTelephoneNumber.toString();
    }

    String getOfficeAreaCode(){
        return _officeTelephoneNumber.getAreaCode();
    }

    void setOfficeAreaCode(String arg){
        _officeTelephoneNumber.setAreaCode(arg);
    }

    String getOfficeNumber(){
        return _officeTelephoneNumber.getNumber();
    }

    void setOfficeNumber(String arg){
        _officeTelephoneNumber.setNumber(arg);
    }

    public Department getDepartment(){
        return _department;
    }

    public Person getManager(){
        return _department.getManager();
    }

    public void setDepartment(Department arg){
        _department = arg;
    }

}
