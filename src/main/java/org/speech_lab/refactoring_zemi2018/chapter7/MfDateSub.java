package org.speech_lab.refactoring_zemi2018.chapter7;

import java.util.Date;

class MfDateSub extends Date {
    MfDateSub(){
        super();
    }

    Date nextDay(){
        return new Date(getYear(), getMonth(), getDate() + 1);
    }
}
