package org.speech_lab.refactoring_zemi2018.chapter1;

class Main {
  public static void main(String[] args){
    Movie harawata = new Movie("死霊のはらわた", Price.REGULAR);
    Movie starwars = new Movie("スター・ウォーズ", Price.REGULAR);

    Movie kametome = new Movie("カメラを止めるな！", Price.NEW_RELEASE);
    Movie kanzou = new Movie("君の肝臓を食べたい", Price.NEW_RELEASE);

    Movie shinchan = new Movie("クレヨンしんちゃん", Price.CHILDRENS);
    Movie precure = new Movie("プリキュア", Price.CHILDRENS);

    Customer foo = new Customer("櫻岡 響");
    foo.addRental(new Rental(starwars, 1));
    foo.addRental(new Rental(harawata, 3));
    foo.addRental(new Rental(kametome, 4));
    foo.addRental(new Rental(precure, 1));

    System.out.println(foo.htmlStatement());
  }
}
